package nxt;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class FibonacciResourceTest {

    @Test
    public void testEndpoint() {
        testFibonacci(0, 0);
        testFibonacci(1, 1);
        testFibonacci(2, 1);
        testFibonacci(3, 2);
        testFibonacci(4, 3);
        testFibonacci(5, 5);
        testFibonacci(6, 8);
    }

    private void testFibonacci(int number, int fibonacci) {
        given()
          .when().get("/fibonacci/"+ number)
          .then()
             .statusCode(200)
             .body(is(String.valueOf(fibonacci)));
    }

}