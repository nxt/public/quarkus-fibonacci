package nxt;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/fibonacci")
public class FibonacciResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{number}")
    public int fibonacci(@PathParam("number") Integer number) {
        if (number == 0) {
            return 0;
        }
        if (number == 1) {
            return 1;
        }
        return fibonacci(number - 2) + fibonacci(number - 1);
    }
}